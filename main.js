import Vue from 'vue'
import App from './App'
import store from './store/store.js'

// 导入请求库
import { $http } from '@escook/request-miniprogram'
// 挂载到uni上面
uni.$http = $http
// 配置请求根路径
$http.baseUrl = 'https://api-hmugo-web.itheima.net'
// $http.baseUrl = 'https://www.uinav.com'

// 请求开始做一些事情
$http.beforeRequest = function(){
	uni.showLoading({
		title:'数据加载中...'
	})
}
// 请求完成之后做一些事情
$http.afterRequest = function () {
  uni.hideLoading()
}
// 封装展示消息提示的方法
uni.$showMsg = function(title='数据加载失败',duration=1500){
	uni.showToast({
		title,
		duration,
		icon:'none'
	})
}

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App,
	store
})
app.$mount()
